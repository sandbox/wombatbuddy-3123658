/**
 * @file
 * Increase a font-size if the language is Hindi and the browser is Firefox.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.IncreaseHindiFont = {
    attach: function (context) {
      $(context).find("*").once("some-arbitrary-key").each(function () {
        var language = $("html").attr("lang");
        if ((language == "hi") && (navigator.userAgent.indexOf("Firefox") > -1)) {
          $(this).css('font-size', '+=0.5');
        }
      });
    }
  };

}(jQuery, Drupal));
